/**
 * Tester for RC servos or brushless motor ESCs,
 * or a PWM controlled LED laser or fan.
 * A potentiometer controls the pulse length or duty cycle of the output.
 * 
 * https://howtomechatronics.com/tutorials/arduino/arduino-brushless-motor-control-tutorial-esc-bldc/
 * For ESC start it with servov value 0 to arm it, then increase to rev up the motor.
 **/

#include <Arduino.h>
#include <Servo.h>
#include <TimerOne.h>

const int analogPin = A3;  // potentiometer input 0-VCC
const int pwmPin = 9;      // pulse output
const int modePin = 5;     // read mode low: pwm, high: servo

int mode = 0;   // 0: pwm mode 1Khz 0-100% duty cycle
                // 1: servo mode 50Hz 1-2mS pulses

Servo myservo;  // create servo object to control a servo

void setup() {
    pinMode(modePin, INPUT_PULLUP);
    mode = digitalRead(modePin);
    if (mode) {
        myservo.attach(pwmPin, 1000, 2000);
    } else {
        Timer1.initialize(1000);  // us period time
    }
    pinMode(LED_BUILTIN_TX, INPUT);
    pinMode(LED_BUILTIN_RX, INPUT);
    // Serial.begin(9600);
}

void loop() {
    int val = analogRead(analogPin);
    if (mode) {
        val = map(val, 0, 1023, 0, 180);
        //  Serial.println(val);
        myservo.write(val);
        delay(40);
    } else {
        Timer1.pwm(pwmPin, val);
        delay(40);
    }
}
